﻿#Import CSV
$VULReportCSV = Import-Csv C:\users\michaelh\Downloads\TEST.csv

#Seperate into Risk Levels

$SeriousVul = $VULReportCSV.where{$_.'Risk Level' -eq "Serious"}
$HighVul = $VULReportCSV.where{$_.'Risk Level' -eq "High"}
$MediumVul = $VULReportCSV.where{$_.'Risk Level' -eq "Medium"}
$InfoVul = $VULReportCSV.where{$_.'Risk Level' -eq "Info"}

## Print Counts

Write-host "Serious: " $SeriousVul.Count -ForegroundColor Magenta
Write-Host "High: " $HighVul.Count -ForegroundColor Red
Write-Host "Medium: " $MediumVul.Count -ForegroundColor Yellow
Write-Host "Info: " $InfoVul.Count



##Complie Informaiton

$Vulnerability = $null
$Vulnerability = $VULReportCSV | select Vulnerability -Unique
$report = @()
foreach($line in $Vulnerability)
{
    $Vulnerabilities = $VULReportCSV.where{$_.Vulnerability -eq $line.Vulnerability}
    #Write Report
    $tempReport = "" | Select Vulnerability, Impact, Remediation, Hosts, RiskLevel
    $tempReport.Vulnerability = ($line.Vulnerability -split '\n')[0]
    $tempReport.Impact = (($Vulnerabilities[0].Consequences -split '\n')[0]).Replace('Impact:','')
    $tempReport.Remediation = $Vulnerabilities[0].Remedation
    $tempReport.Hosts =  $Vulnerabilities.'Host IP'
    $tempReport.RiskLevel = $Vulnerabilities[0].'Risk Level'
    $report += $tempReport


}



#Create HTML Report
$br = "</br>"
$HTMLReport = ""
$HTMLReport += "<h1>Report</h1>"
$HTMLReport += $br
$HTMLReport += $br
$HTMLReport += "<h2>Serious</h2>"
$HTMLReport += $br
$HTMLReport += "<ol>"
foreach($Item in $report.Where{$_.RiskLevel -eq "Serious"})
{

    $HTMLReport += "<li>"
        $HTMLReport += "<ul>"
            $HTMLReport += "<li><b>Vulnerability:</b> "+  $Item.Vulnerability + "</li>"
            $HTMLReport += "<li><b>Impact:</b> "+  $Item.Impact + "</li>"
            $HTMLReport += "<li><b>Remediation:</b> "+  $Item.Remediation + "</li>"
             $HTMLReport += "<li><b>Host(s):</b> <ul>"
            foreach($ip in $Item.Hosts){$HTMLReport += "<li>" + $ip.tostring() + "</li>"}
            $HTMLReport += "</ul></li>"
        $HTMLReport += "</ul>"
    $HTMLReport += "</li>"

}
$HTMLReport += "</ol>"
$HTMLReport += $br
$HTMLReport += "<h2>High</h2>"
$HTMLReport += $br
$HTMLReport += "<ol>"
foreach($Item in $report.Where{$_.RiskLevel -eq "High"})
{

    $HTMLReport += "<li>"
        $HTMLReport += "<ul>"
            $HTMLReport += "<li><b>Vulnerability:</b> "+  $Item.Vulnerability + "</li>"
            $HTMLReport += "<li><b>Impact:</b> "+  $Item.Impact + "</li>"
            $HTMLReport += "<li><b>Remediation:</b> "+  $Item.Remediation + "</li>"
            $HTMLReport += "<li><b>Host(s):</b> <ul>"
            foreach($ip in $Item.Hosts){$HTMLReport += "<li>" + $ip.tostring() + "</li>"}
            $HTMLReport += "</ul></li>"
        $HTMLReport += "</ul>"
    $HTMLReport += "</li>"

}
$HTMLReport += "</ol>"

$HTMLReport += $br
$HTMLReport += "<h2>Medium</h2>"
$HTMLReport += $br
$HTMLReport += "<ol>"
foreach($Item in $report.Where{$_.RiskLevel -eq "Medium"})
{

    $HTMLReport += "<li>"
        $HTMLReport += "<ul>"
            $HTMLReport += "<li><b>Vulnerability:</b> "+  $Item.Vulnerability + "</li>"
            $HTMLReport += "<li><b>Impact:</b> "+  $Item.Impact + "</li>"
            $HTMLReport += "<li><b>Remediation:</b> "+  $Item.Remediation + "</li>"
            $HTMLReport += "<li><b>Host(s):</b> <ul>"
            foreach($ip in $Item.Hosts){$HTMLReport += "<li>" + $ip.tostring() + "</li>"}
            $HTMLReport += "</ul></li>"
        $HTMLReport += "</ul>"
    $HTMLReport += "</li>"

}
$HTMLReport += "</ol>"

$HTMLReport += $br
$HTMLReport += "<h2>Info</h2>"
$HTMLReport += $br
$HTMLReport += "<ol>"
foreach($Item in $report.Where{$_.RiskLevel -eq "Info"})
{

    $HTMLReport += "<li>"
        $HTMLReport += "<ul>"
            $HTMLReport += "<li><b>Vulnerability:</b> "+  $Item.Vulnerability + "</li>"
            $HTMLReport += "<li><b>Impact:</b> "+  $Item.Impact + "</li>"
            $HTMLReport += "<li><b>Remediation:</b> "+  $Item.Remediation + "</li>"
            $HTMLReport += "<li><b>Host(s):</b> <ul>"
            foreach($ip in $Item.Hosts){$HTMLReport += "<li>" + $ip.tostring() + "</li>"}
            $HTMLReport += "</ul></li>"
        $HTMLReport += "</ul>"
    $HTMLReport += "</li>"

}
$HTMLReport += "</ol>"

#Send-MailMessage -To itnotify@linktechconsulting.com -From Automation@linktechconsulting.com -Subject "SBT - Vulnerability Report" -Body $HTMLReport -BodyAsHtml -SmtpServer 192.168.111.30 -Port 25 -Credential (Get-Credential)

$HTMLReport | Out-File C:\Users\michaelh\Desktop\report.html
